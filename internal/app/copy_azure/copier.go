package copy_azure

import (
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
	"time"

	loaderBlob "gitlab.com/republico/library/go/azure/pkg/loader_blob"
	extractorFile "gitlab.com/republico/library/go/extractor/pkg/file"
	utilFile "gitlab.com/republico/library/go/util/pkg/file"
	"gitlab.com/republico/library/go/util/pkg/exception"
)

func Copy() {
	now := fmt.Sprintf("%v", time.Now().UnixNano())	
	pathWork := Config.PathWork
	if pathWork != "" {
		if pathWork[len(pathWork) - 1] != '/' {
			pathWork += "/"
		}
	}
	pathWork += now + "/"

	loaderBlob.Config.Account = os.Getenv(Config.EnvironmentVariableAccount)
	loaderBlob.Config.Key = os.Getenv(Config.EnvironmentVariableKey)

	pathFile := Config.SourceFile
	if len(Config.SourceFile) > 4 {
		if Config.SourceFile[:4] == "http" {
			fileName := Config.SourceFile[strings.LastIndex(Config.SourceFile, "/") + 1:]
			sourceFile := Config.SourceFile
			pathFile = pathWork + fileName

			extractorFile.Download(sourceFile, pathFile)
		}
	}

	data, err := ioutil.ReadFile(pathFile)
	exception.HandlerError(err)

	record := string(data)

	typeFile := strings.ToLower(utilFile.GetExtension(pathFile))

	_, blob := filepath.Split(pathFile)
	blob = strings.Replace(blob, "." + typeFile, "", -1)

	operation := "block"

	loaderBlob.Execute(Config.Container, blob, record, operation)
}