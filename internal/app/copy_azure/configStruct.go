package copy_azure

type ConfigStruct struct {
	EnvironmentVariableAccount string
	EnvironmentVariableKey string
	Container string
	PathFile string
	PathWork string
}

var Config ConfigStruct