package test

import (
	"testing"

	copyAzure "gitlab.com/republico/service/copier_data/internal/app/copy_azure"
)

func TestCopierAzure(t *testing.T) {
	copyAzure.Config = copyAzure.ConfigStruct{
		EnvironmentVariableAccount: "AZURE_ACCOUNT",
		EnvironmentVariableKey: "AZURE_KEY",
		Container: "siconv",
		PathFile: "http://portal.convenios.gov.br/images/docs/CGSIS/csv/siconv_consorcios.csv.zip",
		PathWork: "C:/Teste/republico",
	}

	err := copyAzure.Copy()
	if err != nil {
		t.Errorf("Ocorreu um erro.")
	}
}