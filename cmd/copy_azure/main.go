package main

import (
	"flag"
	"fmt"

	"gitlab.com/republico/library/go/util/pkg/exception"
	copyAzure "gitlab.com/republico/service/copier_data/internal/app/copy_azure"
)

func main() {
	environmentVariableAccount := flag.String("account", "ACCOUNT", "Nome da variável de ambiente com o nome da conta do Azure Storage.")
	environmentVariableKey := flag.String("key", "KEY", "Nome da variável de ambiente com a senha do Azure Storage.")
	container := flag.String("container", "republico", "Nome do container do dado.")
	pathFile := flag.String("pathfile", "", "Endereço da origem do arquivo.")
	pathWork := flag.String("pathwork", "", "Endereço utilizado para o tratamento do dado.")

	flag.Parse()

	copyAzure.Config = copyAzure.ConfigStruct{
		EnvironmentVariableAccount: *environmentVariableAccount,
		EnvironmentVariableKey: *environmentVariableKey,
		Container: *container,
		PathFile: *sourceFile,
		PathWork: *pathWork,
	}

	fmt.Println("Starting receiving messages")

	err := copyAzure.Copy()
	exception.HandlerError(err)

	fmt.Println("Stopping receiving messages")
}