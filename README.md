# Copier

## Depuração

```
go run cmd/pub/main.go -account="AZURE_ACCOUNT" -key="AZURE_KEY" -container="siconv" -pathfile="http://portal.convenios.gov.br/images/docs/CGSIS/csv/siconv_consorcios.csv.zip" -pathwork="C:/Teste/republico"
```

## Teste

### Testes Unitários

```
go test ./test -v -run TestCopierAzure
```

### Testes do executável

```
copy_azure -account="AZURE_ACCOUNT" -key="AZURE_KEY" -container="siconv" -pathfile="http://portal.convenios.gov.br/images/docs/CGSIS/csv/siconv_consorcios.csv.zip" -pathwork="C:/Teste/republico"
```

## Build

```
GOOS=windows GOARCH=amd64 CGO_ENABLED=0 go build -a -ldflags '-extldflags "-static" -s -w' -o bin/copy_azure.exe cmd/copy_azure/*.go
GOOS=linux GOARCH=amd64 CGO_ENABLED=0 go build -a -ldflags '-extldflags "-static" -s -w' -o bin/copy_azure cmd/copy_azure/*.go
```